TARGET_ARM9  = arm9/stage0
TARGET_ARM11 = arm11/stage1

STAGE0_PA = 0x080C3EE0
STAGE1_PA = 0x1FFF4B40
STAGE1_VA = 0xFFFF0B40
MAGIC_PA  = 0x1FFFFFF8
MAGIC_VA  = 0xEFFFFFF8

STAGE0_LD = stage0.ld
STAGE1_LD = stage1.ld

ARM9_SOURCES  = arm9/stage0.s
ARM11_SOURCES = arm11/stage1.s

ARM9_OBJS  = $(ARM9_SOURCES:.s=.o)
ARM11_OBJS = $(ARM11_SOURCES:.s=.o)

PREFIX=arm-none-eabi
CC=$(PREFIX)-gcc
LD=$(PREFIX)-ld
STRIP=$(PREFIX)-strip
OBJCOPY=$(PREFIX)-objcopy

COMMON_FLAGS = -marm -fomit-frame-pointer -mthumb-interwork -mlittle-endian
ARM9_FLAGS   = $(COMMON_FLAGS) -mcpu=arm946e-s -march=armv5te
ARM11_FLAGS  = $(COMMON_FLAGS) -mcpu=mpcore    -march=armv6k

ARM9_LD_FLAGS  = -defsym STAGE0_PA=$(STAGE0_PA) -defsym STAGE1_PA=$(STAGE1_PA) -defsym MAGIC_PA=$(MAGIC_PA)
ARM11_LD_FLAGS = -defsym STAGE1_VA=$(STAGE1_VA) -defsym MAGIC_VA=$(MAGIC_VA)

all: Launcher.dat

Launcher.dat: $(TARGET_ARM9).bin
	python tools/3dsploit.py $(TARGET_ARM9).bin


$(TARGET_ARM9).bin: $(TARGET_ARM11).bin $(ARM9_OBJS)
	$(LD) $(ARM9_LD_FLAGS) -T $(STAGE0_LD) $(ARM9_OBJS) -o $(TARGET_ARM9).elf
	$(STRIP) -s $(TARGET_ARM9).elf
	$(OBJCOPY) -O binary $(TARGET_ARM9).elf $(TARGET_ARM9).bin

$(TARGET_ARM11).bin: $(ARM11_OBJS)
	$(LD) $(ARM11_LD_FLAGS) -T $(STAGE1_LD) $(ARM11_OBJS) -o $(TARGET_ARM11).elf
	$(STRIP) -s $(TARGET_ARM11).elf
	$(OBJCOPY) -O binary $(TARGET_ARM11).elf $(TARGET_ARM11).bin

$(ARM9_OBJS): $(ARM9_SOURCES)
	$(PREFIX)-gcc $(ARM9_FLAGS) -c $< -o $@

$(ARM11_OBJS): $(ARM11_SOURCES)
	$(PREFIX)-gcc $(ARM11_FLAGS) -c $< -o $@

copy: Launcher.dat
	rm -f ~/Dropbox/3dsarm11/eleven-arms/Launcher.dat
	cp Launcher.dat ~/Dropbox/3dsarm11/eleven-arms

clean:
	@rm -rf $(ARM9_OBJS) $(ARM11_OBJS) $(TARGET_ARM9).bin $(TARGET_ARM11).bin \
		$(TARGET_ARM9).elf $(TARGET_ARM11).elf Launcher.dat $(TARGET).elf
