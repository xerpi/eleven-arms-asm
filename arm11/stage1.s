.section .text
.arm
.balign 4
.global _start
_start:
    @get ARM11 core ID
    mrc p15, 0, r1, c0, c0, 5
    and r1, r1, #0b1

    @if not core 0 (aka appcore), return
    cmp r1, #0
    bne exit

    @disable IRQ interrupts
    mrs r2, cpsr
    orr r2, r2, #0x80  @IRQ
    msr cpsr_c, r2

    @tell ARM9 we have ARM11 appcore code exec
    mov r0, #0xEFFFFFF8
    add r1, r1, #1
    str r1, [r0]
    
    @wait for the ARM9 response (MAGIC=5)...
    mov r2, #0
wait_ARM9:
    mcr p15, 0, r2, c7, c14, 0  @Clean and Invalidate Entire Data Cache
    mcr p15, 0, r2, c7, c10, 4  @Drain Synchronization Barrier
    ldr r1, [r0]
    cmp r1, #5
    bne wait_ARM9

    @here ARM9 has written "svc 0x03" to a XN disabled region
    @aka 0xFFFF0D00 VA, so let's go there!
    @jump to 0xFFFF0D00!
    mov lr, #0xFF000000
    orr lr, #0x00FF0000
    orr lr, #0x00000D00
    
    @set custom spsr
    mrs r0, spsr
    @set usermode (0b10000)
    bic r0, r0, #0b01111
    orr r0, r0, #0b10000
    @enable IRQ interrupts
    bic r0, r0, #0x80  @IRQ
    msr spsr_cxsf, r0
    
    mov r0, #0xEFFFFFF8
    mov r1, #20
    str r1, [r0]
    mov r2, #0
    mcr p15, 0, r2, c7, c14, 0  @Clean and Invalidate Entire Data Cache
    mcr p15, 0, r2, c7, c10, 4  @Drain Synchronization Barrier
    
    @return to userland...
    movs pc, lr

exit:
    @what's next ¿?
    bx lr
