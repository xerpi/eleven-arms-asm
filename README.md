# README #

### What is this repository for? ###

* This project aims to get ARM11 code exec from ARM9 code exec

### How do I get set up? ###

* You need arm-none-eabi compiler and python
* Type "make" and you should get the Launcher.dat

Many thanks to Vappy, St4rk and n1ghty for testing!
Also thanks to profi200, plutoo, megazig and all the #3dsdev people
for answering my nooby questions :D
